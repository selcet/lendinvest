### LENDINVEST

`npm install` - installs dependencies and symlinks subpackages

`npm run dev` - build project to dist folder and run development server on localhost

`npm run lint` - runs linter across src folder

`npm run sass` - runs parcel sass compiler
