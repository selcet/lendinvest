import React from "react";
import ReactDOM from "react-dom";
import { Link } from "@reach/router";

import { Loans } from "./Loans";

class App extends React.Component {
  render() {
    return (
      <div className="app app__wrapper">
        <header className="app__header">
          <Link className="app__logo" to="/" title="Go to main page" />
        </header>

        <section className="app__content">
          <h1 className="app__title">Current Loans</h1>
          <Loans />
        </section>
      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("root"));
