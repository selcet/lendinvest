import React from "react";

import Modal from "./Modal";

export class Loan extends React.Component {
  state = { showModal: false };

  toggleModal = () => this.setState({ showModal: !this.state.showModal });

  submitModal = () => {
    const loanInput = document.querySelector(".loan__invest-input");

    if (loanInput.value && loanInput.value != 0) {
      // eslint-disable-next-line no-console
      console.log("value", loanInput.value);
    } else {
      // eslint-disable-next-line no-console
      console.log("value empty", loanInput.value);
    }
  };

  render() {
    const { id, title, available, amount } = this.props;

    return (
      <div className="loan">
        <div className="loan__header">
          <div className="loan__title">Loan Name: {id}</div>
        </div>

        <div className="loan__content">
          <div className="loan__left-part">
            <div className="loan__text">{title}</div>
            <div className="loan__loan-amount">Loan Amount: {available}</div>
            <div className="loan__current-amount">Current Amount: {amount}</div>
          </div>

          <div className="loan__right-part">
            <button className="loan__button" onClick={this.toggleModal}>
              Invest
            </button>
          </div>
        </div>

        {this.state.showModal && (
          <Modal additionalClass="modal_active">
            <h2 className="modal__title">
              Invest in Loan
              {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events */}
              <div
                role="button"
                tabIndex="0"
                className="modal__close-button"
                onClick={this.toggleModal}
              />
            </h2>
            <div className="modal__sub-title">{title}</div>

            <div className="modal__content">
              <div className="loan__available-amount">
                Amount available: {available}
              </div>
              <div className="loan__end-period">
                Loan ends in: 1 month 10 days
              </div>
              <form onSubmit={this.submitModal} className="loan__invest-amount">
                <div className="loan__invest-title">Investment amount (£)</div>
                <input
                  defaultValue="0"
                  type="number"
                  placeholder="enter amount"
                  className="loan__invest-input"
                />
                <input className="loan__button" type="submit" value="Submit" />
              </form>
            </div>
          </Modal>
        )}
      </div>
    );
  }
}
