import React from "react";

import LoansJson from "./current-loans.json";
import { Loan } from "./Loan";

export class Loans extends React.Component {
  state = {
    data: LoansJson
  };

  render() {
    const loans = this.state.data.loans;

    return (
      <div className="loans">
        {loans.map(loan => {
          return (
            <Loan
              key={loan.id}
              id={loan.id}
              title={loan.title}
              tranche={loan.tranche}
              available={loan.available}
              amount={loan.amount}
            />
          );
        })}
      </div>
    );
  }
}
