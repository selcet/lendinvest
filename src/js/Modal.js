import React from "react";
import { createPortal } from "react-dom";

const modalRoot = document.getElementById("modal");

class Modal extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.modalWindow = document.createElement("div");
    this.modalOverlay = document.createElement("div");
    this.modalWindow.classList = "modal__container";
    this.modalOverlay.classList = "modal__overlay";
  }

  componentDidMount() {
    modalRoot.classList.add(this.props.additionalClass);
    modalRoot.appendChild(this.modalOverlay);
    modalRoot.appendChild(this.modalWindow);
  }

  componentWillUnmount() {
    modalRoot.classList.remove(this.props.additionalClass);
    modalRoot.removeChild(this.modalOverlay);
    modalRoot.removeChild(this.modalWindow);
  }

  render() {
    return createPortal(this.props.children, this.modalWindow);
  }
}

export default Modal;
